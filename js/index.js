let bg = document.querySelector('.bg'),
    modal = document.querySelector('.modal'),
    dataAll = document.querySelectorAll('*[data-type]'),
    dataActive = document.querySelectorAll('*[data-active]'),
    krestik = document.querySelector('.krestik'),
    shit = document.querySelector('.shit'),
    forms = document.querySelectorAll('form'),
    inputs = document.querySelectorAll('input'),
    selects = document.querySelectorAll('select'),
    indexCont = document.querySelector('.index-cont'),
    tableCont = document.querySelector('.table-cont')

function manageClasses(bool) {
    if (bool) {
        modal.classList.add('active')
        bg.classList.add('activeBg')
    } else {
        modal.classList.remove('active')
        bg.classList.remove('activeBg')
    }
}


function renderTable() {}


dataAll.forEach((item, index) => {
    item.onclick = () => {
        manageClasses(true, index)
    }
})

// krestik.onclick = () => {
//     console.log('krestik');
//     manageClasses(false)
// }

bg.onclick = () => {
    manageClasses(false)
}


let arr = [{
        title: "Переписать проект на",
        info: "Quia et suscipit\nsuscipit recusandae expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
        globalTime: "21.10.21",
        localeTime: "14:31",
        status: "Не выполнено",
    },
    {
        title: "Переписать проект на",
        info: "Quia et suscipit\nsuscipit recusandae  expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
        globalTime: "21.10.21",
        localeTime: "14:31",
        status: "Не выполнено",
    },
    {
        title: "Переписать проект на",
        info: "Quia et suscipit\nsuscipit recusandae expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
        globalTime: "21.10.21",
        localeTime: "14:31",
        status: "Не выполнено",
    },

] || JSON.parse(localStorage.copyArr)


arr.forEach((item, index) => {
    item.id = index
})

localStorage.copyArr ? console.log() : localStorage.copyArr = JSON.stringify(arr)

function renderIndex(array) {
    indexCont.innerHTML = ''
    array.forEach((item, index) => {
        block = document.createElement('div')
        p1 = document.createElement('p')
        p2 = document.createElement('p')
        p3 = document.createElement('p')
        p4 = document.createElement('p')
        p5 = document.createElement('p')
        btn = document.createElement('img')
        btn2 = document.createElement('img')
        cascad = document.createElement('div')


        block.classList.add('item')
        p1.classList.add("item-p")
        p2.classList.add("item-p")
        p3.classList.add("item-p")
        p4.classList.add("item-p")
        p5.classList.add("item-p")
        p1.innerHTML = `${index + 1}.${item.title}`
        p2.innerHTML = `${item.info}`
        p3.innerHTML = `${item.globalTime}`
        p4.innerHTML = `${item.localeTime}`
        p5.innerHTML = `${item.status}`
        cascad.classList.add('cascad')
        btn.setAttribute("src", "./public/tododelete.png")
        btn2.setAttribute("src", "./public/todoedit.png")

        btn.classList.add('delete-icon')
        btn2.classList.add('edit-icon')


        btn.onclick = () => {
            array.forEach((table, index) => {
                if (table.id == item.id) {
                    console.log(index);
                    array.splice(index, 1)
                }
            })
            localStorage.copyArr = JSON.stringify(array)
            renderIndex(JSON.parse(localStorage.copyArr))
        }

        if (item.status == "Не выполнено") {

            p5.classList.add('fail')
        }
        if (item.status == "Готово") {

            p5.classList.add('done')
        }
        if (item.status == "В прогрессе") {

            p5.classList.add("progress")
        }

        block.append(p1, p2, p3, p4, p5, btn)
        indexCont.append(block)
    })

}

function renderTable(array) {
    tableCont.innerHTML = ''
    for (let item of array) {
        blockT = document.createElement('div')

        p1 = document.createElement('p')
        p2 = document.createElement('p')
        div = document.createElement('div')
        p3 = document.createElement('p')
        p4 = document.createElement('p')
        p5 = document.createElement('p')
        btn = document.createElement('img')
        cont = document.createElement('div')

        blockT.classList.add('item-table')
        p1.classList.add("table-title")
        p2.classList.add("table-title")
        div.classList.add("cont-p")
        p5.classList.add("table-title")
        cont.classList.add('item-table-cont')
        p1.innerHTML = `${item.title}`
        p2.innerHTML = `${item.info}`
        p3.innerHTML = `${item.globalTime}`
        p4.innerHTML = `${item.localeTime}`
        p5.innerHTML = `${item.status}`

        btn.classList.add('delete-icon-table')
        btn.setAttribute("src", "./public/tododelete.png")
        if (item.status == "Не выполнено") {

            p5.classList.add('fail')
        }
        if (item.status == "Готово") {

            p5.classList.add('done')
        }
        if (item.status == "В прогрессе") {

            p5.classList.add("progress")
        }
        btn.onclick = () => {
            console.log(item.id);
            // arr.splice(item.id, 1)
            array.forEach((table, index) => {
                if (table.id == item.id) {
                    console.log(index);
                    array.splice(index, 1)
                }
            })
            localStorage.copyArr = JSON.stringify(array)
            renderTable(JSON.parse(localStorage.copyArr))
        }
        div.append(p3, p4)
        cont.append(p5, btn)
        blockT.append(p1, p2, div, cont)
        tableCont.append(blockT)
    }

}




forms.forEach(item => {
    item.onsubmit = (event) => {
        event.preventDefault()


        let obj = {}
        let fm = new FormData(item)

        fm.forEach((value, key) => {
            obj[key] = value
        })
        arr = JSON.parse(localStorage.copyArr)
        arr.push(obj)
        arr.forEach((item, index) => {
            item.id = index
        })
        localStorage.copyArr = JSON.stringify(arr)
        if (document.querySelector('.table')) {
            renderTable(JSON.parse(localStorage.copyArr))
        } else {
            renderIndex(JSON.parse(localStorage.copyArr))
        }
        inputs.forEach(item => {
            item.value = ''
        })
        manageClasses(false)
    }
})

if (document.querySelector('.index')) {
    if (localStorage.copyArr) {
        renderIndex(JSON.parse(localStorage.copyArr))

    }
    if (localStorage.copyArr == false) {
        renderIndex(arr)
    }

}

if (document.querySelector('.table')) {
    if (localStorage.copyArr) {
        renderTable(JSON.parse(localStorage.copyArr))

    }
    if (localStorage.copyArr == false) {
        renderTable(arr)
    }

}
// localStorage.copyArr ? renderIndex(JSON.parse(localStorage.copyArr)) : renderIndex(arr)